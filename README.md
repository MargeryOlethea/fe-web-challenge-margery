# Star Wars Explorer

[![License](https://img.shields.io/gitlab/license/MargeryOlethea/fe-web-challenge-margery)](https://gitlab.com/MargeryOlethea/fe-web-challenge-margery/-/blob/main/LICENSE)

Explore the Star Wars universe with this web app! Find films, characters, and planets easily.

### [Check out the Star Wars Explorer app here!](https://fe-web-challenge-margery-drab.vercel.app/)

## Description

This app allows you to search for films, characters (people), and planets from the Star Wars universe. You can easily navigate through search results with pagination.

**Stacks Used:**

- React
- Tailwind CSS
- DaisyUI

**API Source:** [SWAPI - The Star Wars API](https://swapi.py4e.com)

## App Look

### Home Screen

![HomeScreen](./fe-starwars/public/images/filmScreen.png)

### Modal

![HomeScreenModal](./fe-starwars/public/images/filmScreen3.png)

### Navigation Bar Dropdown

![NavBarDropdown](./fe-starwars/public/images/filmScreen2.png)

### Characters Screen

![CharactersScreen](./fe-starwars/public/images/characterScreen.png)

### Planet Screen

![PlanetScreen](./fe-starwars/public/images/planetsScreen.png)

### Pagination

![Pagination](./fe-starwars/public/images/planetsScreen2.png)

### Empty List

![EmptyList](./fe-starwars/public/images/filmScreen4.png)

## Installation

1. Clone the repository: `git clone https://gitlab.com/MargeryOlethea/fe-web-challenge-margery.git`
2. Navigate to the project directory: `cd fe-web-challenge-margery`
3. Navigate to the front-end folder: `cd fe-starwars`
4. Install dependencies: `npm install` (or `yarn install` if using Yarn)

## Usage

1. Start the development server: `npm run dev`
2. The application will be accessible at [http://localhost:5173](http://localhost:5173) (default port, might vary)
