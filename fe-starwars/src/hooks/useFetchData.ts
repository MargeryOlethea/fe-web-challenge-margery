import { useState, useEffect, useContext } from "react";
import { FetchResponseType } from "../types/data.types";
import toast from "react-hot-toast";
import { SearchContext } from "../contexts/SearchContext";

const useFetchData = <T>(endPoint: string) => {
  const [data, setData] = useState<FetchResponseType<T> | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const { search } = useContext(SearchContext);

  useEffect(() => {
    const fetchData = async () => {
      const baseAPI = "https://swapi.py4e.com/api/";
      try {
        const response = await fetch(`${baseAPI}${endPoint}`);
        const jsonData = await response.json();
        setData(jsonData);
      } catch (error) {
        if (error instanceof Error) {
          console.error(error.message);
          toast.error("Error fetching data", { icon: "🔥" });
        } else {
          console.error(error);
          toast.error("Error fetching data", { icon: "🔥" });
        }
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [endPoint, search]);

  return { data, loading };
};

export default useFetchData;
