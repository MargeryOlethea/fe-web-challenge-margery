import React, { createContext, useState } from "react";

interface SearchContextValue {
  search: string;
  setSearch: (search: string) => void;
}

export const SearchContext = createContext<SearchContextValue>({
  search: "",
  setSearch: () => {},
});

export const SearchProvider: React.FC<{ children: React.ReactNode }> = ({
  children,
}) => {
  const [search, setSearch] = useState("");

  return (
    <SearchContext.Provider value={{ search, setSearch }}>
      {children}
    </SearchContext.Provider>
  );
};
