import { useSearchParams } from "react-router-dom";

const SearchBar = () => {
  const [searchParams, setSearchParams] = useSearchParams();

  const handleSearch = (word: string) => {
    const debounceTime = 750; // in ms
    setTimeout(() => {
      setSearchParams({ ...searchParams, search: word, page: "1" });
    }, debounceTime);
  };
  return (
    <>
      <div className="flex gap-2 items-center justify-start input bg-primary border-accent text-accent">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-4 w-4"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          />
        </svg>
        <input
          className="placeholder:text-accent text-accent bg-transparent"
          type="text"
          placeholder="Search..."
          onChange={(e) => handleSearch(e.target.value)}
        />
      </div>
    </>
  );
};

export default SearchBar;
