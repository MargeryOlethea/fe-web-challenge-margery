import { useSearchParams } from "react-router-dom";

const Pagination = ({
  previous,
  next,
}: {
  previous: string | null;
  next: string | null;
}) => {
  const [searchParams, setSearchParams] = useSearchParams();

  const pageParam = Number(searchParams.get("page")) || 1;
  const searchParam = searchParams.get("search") || "";
  const handlePrevious = () => {
    setSearchParams({
      search: searchParam,
      page: `${pageParam - 1}`,
    });
  };

  const handleNext = () => {
    setSearchParams({
      search: searchParam,
      page: `${pageParam + 1}`,
    });
  };

  return (
    <div className="flex justify-between mt-10">
      <button
        className="join-item btn btn-primary disabled:cursor-not-allowed disabled:bg-[#42549e] disabled:text-slate-900 text-white"
        disabled={previous === null}
        onClick={handlePrevious}
      >
        Previous Page
      </button>
      <button
        className="join-item btn btn-primary disabled:cursor-not-allowed disabled:bg-[#42549e] disabled:text-slate-900 text-white"
        disabled={next === null}
        onClick={handleNext}
      >
        Next Page
      </button>
    </div>
  );
};

export default Pagination;
