import { Link, useLocation } from "react-router-dom";
import SearchBar from "./SearchBar";

const Header = () => {
  const navItems = [
    { name: "Films", path: "/" },
    { name: "Characters", path: "/characters" },
    { name: "Planets", path: "/planets" },
  ];

  const location = useLocation();

  return (
    <nav className="bg-primary">
      <div className="navbar max-w-screen-xl mx-auto">
        <div className="navbar-start">
          <div className="dropdown">
            <div
              tabIndex={0}
              role="button"
              className="btn btn-ghost btn-circle text-accent"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-5 w-5"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M4 6h16M4 12h16M4 18h7"
                />
              </svg>
            </div>
            <ul
              tabIndex={0}
              className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52"
            >
              {navItems.map((item) => (
                <li
                  key={item.name}
                  className={`${
                    location.pathname === item.path && "bg-primary text-accent"
                  } font-bold`}
                >
                  <Link to={item.path}>{item.name}</Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="navbar-center">
          <Link
            to="/"
            className="text-xl font-starJedi hover:scale-105 text-accent transition-all ease-in-out"
          >
            Star Wars Explorer
          </Link>
        </div>
        <div className="navbar-end">
          <SearchBar />
        </div>
      </div>
    </nav>
  );
};

export default Header;
