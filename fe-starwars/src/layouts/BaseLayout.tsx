import { Outlet } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import { Toaster } from "react-hot-toast";

const BaseLayout = () => {
  return (
    <>
      <main className="min-h-screen justify-between flex-col flex">
        <Header />
        <Outlet />
        <Toaster position="bottom-right" />
        <Footer />
      </main>
    </>
  );
};

export default BaseLayout;
