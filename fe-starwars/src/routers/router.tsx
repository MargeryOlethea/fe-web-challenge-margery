import { createBrowserRouter } from "react-router-dom";
import BaseLayout from "../layouts/BaseLayout";
import FilmPage from "../pages/FilmPage";
import PeoplePage from "../pages/PeoplePage";
import PlanetPage from "../pages/PlanetPage";

export const router = createBrowserRouter([
  {
    element: <BaseLayout />,
    children: [
      { path: "/", element: <FilmPage /> },
      { path: "/characters", element: <PeoplePage /> },
      { path: "/planets", element: <PlanetPage /> },
    ],
  },
]);
