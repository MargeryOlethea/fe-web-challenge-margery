import useFetchData from "../hooks/useFetchData";
import { PeopleType } from "../types/data.types";
import Loading from "../components/Loading";
import Pagination from "../layouts/components/Pagination";
import { useSearchParams } from "react-router-dom";
import PersonCard from "../components/PersonCard";
import Heading from "../components/Heading";

const PeoplePage = () => {
  const [searchParams] = useSearchParams();
  const pageParam = Number(searchParams.get("page")) || 1;
  const searchParam = searchParams.get("search") || "";

  const { data, loading } = useFetchData<PeopleType>(
    `people/?page=${pageParam}&search=${searchParam}`,
  );

  return (
    <main className="max-w-screen-xl w-full mx-auto py-10">
      <Heading text="Star Wars Characters" />
      {loading ? (
        <Loading />
      ) : (
        data && (
          <>
            {data.results.length > 0 ? (
              <>
                <div className="grid grid-cols-5 gap-4">
                  {data.results.map((person) => (
                    <PersonCard person={person} />
                  ))}
                </div>

                <Pagination previous={data?.previous} next={data?.next} />
              </>
            ) : (
              <div className="text-center text-accent w-full">
                No Character Found
              </div>
            )}
          </>
        )
      )}
    </main>
  );
};

export default PeoplePage;
