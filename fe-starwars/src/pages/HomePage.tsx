import { Link } from "react-router-dom";

const HomePage = () => {
  return (
    <main className="min-h-screen min-w-screen flex justify-center items-center">
      <div className="card w-96 bg-primary text-neutral-content">
        <div className="card-body items-center text-center">
          <h1 className="card-title font-starJedi text-3xl tracking-widest text-accent">
            Star Wars Explorer
          </h1>

          <div className="dropdown dropdown-hover">
            <div
              tabIndex={0}
              role="button"
              className="btn m-1 bg-white text-primary border-none"
            >
              Start Exploring
            </div>
            <ul
              tabIndex={0}
              className="dropdown-content z-[1] menu p-2 shadow bg-accent text-primary font-bold rounded-box w-52"
            >
              <li>
                <Link to="/films">Films</Link>
              </li>
              <li>
                <Link to="/people">People</Link>
              </li>
              <li>
                <Link to="/planets">Planets</Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </main>
  );
};

export default HomePage;
