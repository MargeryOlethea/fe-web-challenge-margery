import FilmCard from "../components/FilmCard";
import Heading from "../components/Heading";
import Loading from "../components/Loading";
import useFetchData from "../hooks/useFetchData";
import Pagination from "../layouts/components/Pagination";
import { FilmType } from "../types/data.types";
import { useSearchParams } from "react-router-dom";

const FilmPage = () => {
  const [searchParams] = useSearchParams();
  const pageParam = Number(searchParams.get("page")) || 1;
  const searchParam = searchParams.get("search") || "";

  const { data, loading } = useFetchData<FilmType>(
    `films/?page=${pageParam}&search=${searchParam}`,
  );

  return (
    <main className="max-w-screen-xl w-full h-full mx-auto py-10">
      <Heading text="Star Wars Films" />
      {loading ? (
        <Loading />
      ) : (
        data && (
          <>
            {data.results.length > 0 ? (
              <>
                <div className="grid grid-cols-3 gap-4 w-full">
                  {data.results.map((film) => (
                    <FilmCard key={film.title} film={film} />
                  ))}
                </div>

                <Pagination previous={data?.previous} next={data?.next} />
              </>
            ) : (
              <div className="text-center text-accent w-full">
                No Film Found
              </div>
            )}
          </>
        )
      )}
    </main>
  );
};

export default FilmPage;
