import { useSearchParams } from "react-router-dom";
import { PlanetType } from "../types/data.types";
import useFetchData from "../hooks/useFetchData";
import PlanetCard from "../components/PlanetCard";
import Pagination from "../layouts/components/Pagination";
import Loading from "../components/Loading";
import Heading from "../components/Heading";

const PlanetPage = () => {
  const [searchParams] = useSearchParams();
  const pageParam = Number(searchParams.get("page")) || 1;
  const searchParam = searchParams.get("search") || "";

  const { data, loading } = useFetchData<PlanetType>(
    `planets/?page=${pageParam}&search=${searchParam}`,
  );
  return (
    <main className="max-w-screen-xl w-full mx-auto py-10">
      <Heading text="Star Wars Planets" />
      {loading ? (
        <Loading />
      ) : (
        data && (
          <>
            {data.results.length > 0 ? (
              <>
                <div className="grid grid-cols-5 gap-4">
                  {data.results.map((planet) => (
                    <PlanetCard planet={planet} />
                  ))}
                </div>
                <Pagination previous={data?.previous} next={data?.next} />
              </>
            ) : (
              <div className="text-center text-accent w-full">
                No Planet Found
              </div>
            )}
          </>
        )
      )}
    </main>
  );
};

export default PlanetPage;
