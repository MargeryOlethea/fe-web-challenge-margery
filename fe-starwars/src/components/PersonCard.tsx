import { PeopleType } from "../types/data.types";

const PersonCard = ({ person }: { person: PeopleType }) => {
  return (
    <div className="card w-full bg-neutral text-neutral-content">
      <div className="card-body">
        <h2 className="card-title font-starJedi tracking-widest h-10">
          {person.name}
        </h2>
        <div className="my-5">
          {/* birth year */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Birth Year</span>
            <span className="font-bold">{person.birth_year}</span>
          </div>

          {/* eye color */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Eye Color</span>
            <span className="font-bold">{person.eye_color}</span>
          </div>

          {/* gender */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Gender</span>
            <span className="font-bold">{person.gender}</span>
          </div>

          {/* hair color */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Hair Color</span>
            <span className="font-bold">{person.hair_color}</span>
          </div>

          {/* height */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Height</span>
            <span className="font-bold">
              {" "}
              {person.height !== "unknown" ? person.height + " M" : "unknown"}
            </span>
          </div>

          {/* mass */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Mass</span>
            <span className="font-bold">
              {person.mass !== "unknown" ? person.mass + " KG" : "unknown"}
            </span>
          </div>

          {/* skin color */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Skin Color</span>
            <span className="font-bold text-right">{person.skin_color}</span>
          </div>
        </div>
        {/* TO BE INSERTED: HOME WORLD, FILMS */}
      </div>
    </div>
  );
};

export default PersonCard;
