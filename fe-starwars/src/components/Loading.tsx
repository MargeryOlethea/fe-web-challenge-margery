const Loading = ({ color = "primary" }: { color?: string }) => {
  return (
    <div className="h-full w-full flex justify-center items-center">
      <div className={`loading loading-dots loading-lg bg-${color}`}></div>
    </div>
  );
};

export default Loading;
