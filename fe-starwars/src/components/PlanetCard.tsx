import { numberFormatter, numberSplitter } from "../helpers/NumberModifier";
import { PlanetType } from "../types/data.types";

const PlanetCard = ({ planet }: { planet: PlanetType }) => {
  return (
    <div className="card w-full bg-neutral text-neutral-content">
      <div className="card-body">
        <h2 className="card-title font-starJedi tracking-widest">
          {planet.name}
        </h2>
        <div className="my-5">
          {/* rotation period */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Rotation Period</span>
            <span className="font-bold text-sm">
              {planet.rotation_period} Hrs
            </span>
          </div>
          {/* orbital period */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Orbital Period</span>
            <span className="font-bold text-sm">{planet.orbital_period} D</span>
          </div>
          {/* diameter */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Diameter</span>
            <span className="font-bold text-sm">
              {planet.diameter !== "unknown"
                ? numberSplitter(+planet.diameter) + " KM"
                : "unknown"}
            </span>
          </div>
          {/* climate */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Climate</span>
            <span
              className={`font-bold text-right ${
                planet.climate.length > 10 ? "text-xs" : "text-sm"
              }`}
            >
              {planet.climate}
            </span>
          </div>
          {/* gravity */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Gravity</span>
            <span
              className={`font-bold text-right ${
                planet.gravity.length > 10 ? "text-xs" : "text-sm"
              }`}
            >
              {planet.gravity}
            </span>
          </div>
          {/* terrain */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Terrain</span>
            <span
              className={`font-bold text-right ${
                planet.terrain.length > 10 ? "text-xs" : "text-sm"
              }`}
            >
              {planet.terrain}
            </span>
          </div>
          {/* surface water */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Surface Water</span>
            <span className="font-bold text-sm">{planet.surface_water}%</span>
          </div>
          {/* population */}
          <div className="flex justify-between items-center my-2">
            <span className="text-xs text-white">Population</span>
            <span className="font-bold text-sm">
              {planet.population !== "unknown"
                ? numberFormatter(+planet.population)
                : "unknown"}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PlanetCard;
