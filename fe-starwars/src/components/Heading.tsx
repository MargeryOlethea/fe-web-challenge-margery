const Heading = ({ text }: { text: string }) => {
  return (
    <h1 className="text-accent text-3xl font-starJedi tracking-widest ml-3 mb-10">
      {text}
    </h1>
  );
};

export default Heading;
