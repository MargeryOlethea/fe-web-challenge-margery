import { dateFormatter } from "../helpers/DateModifier";
import { FilmType } from "../types/data.types";
import FilmModal from "./FilmModal";

const FilmCard = ({ film }: { film: FilmType }) => {
  return (
    <div className="card w-full bg-neutral text-neutral-content">
      <div className="card-body">
        <h2 className="card-title font-starJedi tracking-widest">
          {film.title}
        </h2>

        <div className="my-5">
          {/* director */}
          <p className="text-xs text-white">Director</p>
          <p className="font-bold mb-5">{film.director}</p>

          {/* producer */}
          <p className="text-xs text-white">Producer</p>
          <p className="font-bold mb-5">{film.producer}</p>

          {/* release date */}
          <p className="text-xs text-white">Release Date</p>
          <p className="font-bold mb-5">{dateFormatter(film.release_date)}</p>

          {/* opening crawl */}
          <p className="text-xs text-white">Opening Crawl</p>
          <p className="font-bold">
            {film.opening_crawl.substring(0, 100) + "..."}
          </p>
        </div>
        {/* TO BE INSERTED: PLANETS, CHARACTERS */}
        <div className="card-actions justify-end">
          <label
            className="btn btn-primary"
            htmlFor={`film_modal_${film.title}`}
          >
            See More
          </label>
        </div>
      </div>

      <FilmModal film={film} />
    </div>
  );
};

export default FilmCard;
