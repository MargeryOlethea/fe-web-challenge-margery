import { useEffect, useState } from "react";
import { FilmType, PeopleType, PlanetType } from "../types/data.types";
import toast from "react-hot-toast";
import Loading from "./Loading";

function FilmModal({ film }: { film: FilmType }) {
  const [characters, setCharacters] = useState<string[]>([]);
  const [planets, setPlanets] = useState<string[]>([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchCharacterData = async () => {
      setLoading(true);
      try {
        const characterData = await Promise.all(
          film.characters.map(async (characterUrl) => {
            const response = await fetch(characterUrl);
            const data: PeopleType = await response.json();
            return data.name;
          }),
        );
        setCharacters(characterData);
      } catch (error) {
        if (error instanceof Error) {
          console.error(error.message);
          toast.error("Error fetching character data", { icon: "🔥" });
        } else {
          console.error(error);
          toast.error("Error fetching character data", { icon: "🔥" });
        }
      } finally {
        setLoading(false);
      }
    };

    const fetchPlanetData = async () => {
      setLoading(true);
      try {
        const planetData = await Promise.all(
          film.planets.map(async (planetUrl) => {
            const response = await fetch(planetUrl);
            const data: PlanetType = await response.json();
            return data.name;
          }),
        );
        setPlanets(planetData);
      } catch (error) {
        if (error instanceof Error) {
          console.error(error.message);
          toast.error("Error fetching character data", { icon: "🔥" });
        } else {
          console.error(error);
          toast.error("Error fetching character data", { icon: "🔥" });
        }
      }
    };
    fetchCharacterData();
    fetchPlanetData();
  }, [film]);

  const planetList = planets.join(", ");
  const characterList = characters.join(", ");
  return (
    <>
      <input
        type="checkbox"
        id={`film_modal_${film.title}`}
        className="modal-toggle"
      />
      <div className="modal" role="dialog">
        <div className="modal-box bg-primary">
          {loading ? (
            <Loading color="accent" />
          ) : (
            <>
              <h2 className="card-title font-starJedi tracking-widest">
                {film.title}
              </h2>

              <div className="my-5">
                {/* director */}
                <p className="text-xs text-white">Director</p>
                <p className="font-bold mb-5">{film.director}</p>

                {/* producer */}
                <p className="text-xs text-white">Producer</p>
                <p className="font-bold mb-5">{film.producer}</p>

                {/* release date */}
                <p className="text-xs text-white">Release Date</p>
                <p className="font-bold mb-5">{film.release_date}</p>

                {/* planets */}
                <p className="text-xs text-white">Planets</p>
                <p className="font-bold mb-5">{planetList}</p>

                {/* planets */}
                <p className="text-xs text-white">Characters</p>
                <p className="font-bold mb-5 text-sm">{characterList}</p>

                {/* opening crawl */}
                <p className="text-xs text-white">Opening Crawl</p>
                <p className="font-bold text-sm">{film.opening_crawl}</p>
              </div>
            </>
          )}
        </div>

        <label className="modal-backdrop" htmlFor={`film_modal_${film.title}`}>
          Close
        </label>
      </div>
    </>
  );
}

export default FilmModal;
