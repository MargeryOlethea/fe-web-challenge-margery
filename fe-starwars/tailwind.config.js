/** @type {import('tailwindcss').Config} */
import daisyui from "daisyui";
import cyberpunk from "daisyui/src/theming/themes";

export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: { fontFamily: { starJedi: ["Star Jedi"] } },
  },
  daisyui: {
    themes: [
      {
        light: {
          ...cyberpunk["cyberpunk"],
          accent: "#FFF80A",
          primary: "#D61C4E",
          secondary: "#FEB139",
          background: "#293462",
          disabled: "#42549e",
        },
      },
    ],
  },
  plugins: [daisyui],
};
